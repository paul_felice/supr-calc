import { ADD_RESULT } from './types';

// gets homepage data
export const addResult = payload => dispatch =>
	dispatch({
		type: ADD_RESULT,
		payload,
	});
