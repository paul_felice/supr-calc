import { ADD_RESULT } from "./types";

const initialState = [];

export default (state = initialState, action) => {
	switch (action.type) {
		case ADD_RESULT:
			return [action.payload, ...state.slice(0, 4)];
		default:
			return state;
	}
};
