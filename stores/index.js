import { createStore, applyMiddleware, compose, combineReducers } from 'redux';
import thunk from 'redux-thunk';

// Combine all reducers
import history from './history/reducer';
import input from './input/reducer';

export const initStore = (initialState = {}) =>
	createStore(
		combineReducers({
			history,
			input
		}),
		initialState,
		compose(
			applyMiddleware(thunk),
			typeof window !== 'undefined' && window.devToolsExtension
				? window.devToolsExtension()
				: f => f
		)
	);