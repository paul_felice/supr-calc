import { Subject, from } from 'rxjs';
import {
	map,
	debounceTime,
	distinctUntilChanged,
	catchError,
	switchMap,
	filter,
} from 'rxjs/operators';

import { ADD_INPUT, RESET, SET_RESULT, SET_ERROR } from './types';
import { evaluate } from '../../logic/evaluator.mjs';
import { ADD_RESULT } from '../history/types';

export const reset = () => dispatch =>
	dispatch({
		type: RESET,
	});

const subject$ = new Subject();
const stream = subject$.pipe(
	debounceTime(300),
	map(x => x.trim()),
	distinctUntilChanged(),
	filter(y => typeof y === 'string' && y.length > 0)
);

export const write = payload => (dispatch, getState) => {
	dispatch({
		type: ADD_INPUT,
		payload,
	});
	const subscriber = stream
		.pipe(
			switchMap(expr =>
				from(evaluate(expr)).pipe(
					map(result => ({ expr, result })),
					catchError(err => {
						dispatch({
							type: SET_ERROR,
							payload: err.message,
						});
						subscriber.unsubscribe();
					})
				)
			)
		)
		.subscribe(data => {
			dispatch({
				type: SET_RESULT,
				payload: data,
			});
			dispatch({
				type: ADD_RESULT,
				payload: data,
			});
			subscriber.unsubscribe();
		});
	subject$.next(getState().input.value.join(''));
};
