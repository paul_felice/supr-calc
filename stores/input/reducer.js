import { ADD_INPUT, RESET, SET_RESULT, SET_ERROR } from './types';

const initialState = { value: [], result: null, error: null };

export default (state = initialState, action) => {
	switch (action.type) {
		case ADD_INPUT:
			return (() => {
				let newPayload = action.payload;

				if (newPayload === '⌫') {
					let toRemove = 1;
					if (state.value.length <= 0) toRemove = 0;
					if (['sin', 'cos', 'tan'].find(x => x === state.value[state.value.length - 1] )) toRemove = 2;
					return {
						...state,
						result: null,
						error: null,
						value: [...state.value.slice(0, state.value.length - toRemove)],
					};
				}

				if (['sin', 'cos', 'tan'].find(x => x === action.payload)) {
					newPayload = `${newPayload}(`;
				}
				return {
					...state,
					result: null,
					error: null,
					value: [...state.value, newPayload],
				};
			})();
		case RESET:
			return initialState;
		case SET_RESULT:
			return { ...state, result: action.payload };
		case SET_ERROR:
			return { ...state, error: action.payload };
		default:
			return state;
	}
};
