import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { omit } from 'lodash';
import './styles.scss';

const Container = props => {
	const { className } = props;
	const newProps = {
		...omit(props, 'className'),
		className: classnames(className, 'container'),
	};
	return <div {...newProps} />;
};

const Row = props => {
	const { className } = props;
	const newProps = {
		...omit(props, 'className'),
		className: classnames(className, 'row'),
	};
	return <div {...newProps} />;
};

// TODO make autowidth when width not specified
const Column = props => {
	const { className, desktop, mobile } = props;
	const newProps = {
		...omit(props, 'className', 'desktop', 'mobile'),
		className: classnames(
			className,
			`col-${desktop}`,
			`col-${mobile}-sm`,
		),
	};
	return <div {...newProps} />;
};

Column.propTypes = {
	mobile: PropTypes.number,
	desktop: PropTypes.number,
};

Column.defaultProps = {
	mobile: 12,
	desktop: 12,
};

export { Container, Row, Column };
