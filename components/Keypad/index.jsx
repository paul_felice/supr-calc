import React from 'react';
import { chunk } from 'lodash';
import { connect } from 'react-redux';
// import { List } from 'semantic-ui-react';
import * as actions from '../../stores/input/actions';
import styles from './styles.scss';

const allKeys = [
	0,
	1,
	2,
	3,
	4,
	5,
	6,
	7,
	8,
	9,
	'sin',
	'cos',
	'tan',
	'RAND',
	'+',
	'-',
	'*',
	'/',
	')',
	'⌫'
];

export const Keypad = connect(
	null,
	actions
)(({ write }) => (
	<div className={styles.container}>
		<div className={styles.portrait}>
			<table className={styles.table}>
				<tbody>
					{chunk(allKeys, 4).map((arr, i) => (
						<tr key={i}>
							{arr.map(item => (
								<td onClick={() => write(item)} key={item}>
									{item}
								</td>
							))}
						</tr>
					))}
				</tbody>
			</table>
		</div>
		<div className={styles.landscape}>
			<table className={styles.table}>
				<tbody>
					{chunk(allKeys, 10).map((arr, i) => (
						<tr key={i}>
							{arr.map(item => (
								<td onClick={() => write(item)} key={item}>
									{item}
								</td>
							))}
						</tr>
					))}
				</tbody>
			</table>
		</div>
	</div>
));
