import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { List } from 'semantic-ui-react';

const History = ({ list }) => (
	<List celled>
		{list.map(({ result, expr }) => (
			<List.Item><strong>{expr} | </strong>{result}</List.Item>
		))}
	</List>
);

History.propTypes = {
	list: PropTypes.arrayOf(PropTypes.string),
};

const ConnectedHistory = connect(state => ({
	list: state.history,
}))(History);

export { ConnectedHistory as History };
