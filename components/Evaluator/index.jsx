import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Input, Message } from 'semantic-ui-react';
import * as actions from '../../stores/history/actions';

const Evaluator = ({ input, error, result }) => (
	<div>
		<Input
			placeholder="Enter Expression..."
			value={input}
			fluid
			size="large"
		/>
		{result ? <Message success header="Result" content={result} /> : null}
		{error ? <Message error header="Error" content={error} /> : null}
	</div>
);

Evaluator.propTypes = {
	input: PropTypes.string.isRequired,
	error: PropTypes.string.isRequired,
	result: PropTypes.string.isRequired,
};

const ConnectedEvaluator = connect(
	state => ({
		input: state.input.value.join(''),
		error: state.input.error,
		result: state.input.result ? state.input.result.result : null,
	}),
	actions
)(Evaluator);

export { ConnectedEvaluator as Evaluator };
