import readline from 'readline';
import {
	evaluate
} from './logic/evaluator.mjs';

const rl = readline.createInterface({
	input: process.stdin,
	output: process.stdout,
});

rl.question('Enter an expression\n', expr => {
	rl.close();
	evaluate(expr).then(data => console.log(data)); // eslint-disable-line
});