import readline from 'readline';
import { evaluate } from './logic/evaluator.mjs';

const rl = readline.createInterface({
	input: process.stdin,
	output: process.stdout,
});

rl.question('Enter an expression\n', async expr => {
	try {
		await evaluate(expr);
		console.log('TRUE')
	} catch (e) {
		console.log('FALSE');
	}
	rl.close();
});