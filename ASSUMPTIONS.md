# Assumptions

- This project was developed for latest browsers.
- It requires a stable internet connection.
- Included in the project (as a dependency but not used) is mathjs-expression-parser. It is a lightweight library and in production I would opt for such a library given it is well tested.
- Unfortunately I didn't find time for testing, both unit and E2E but given some time it would be ideal. In any case, I used fail-fast approach.
- It is assumed the trigonometric functions take degrees rather than radians. Given the high numbers in the description of Task 2 and that sin(30 deg) is such an important value:
> (sin(30) + cos(20))
