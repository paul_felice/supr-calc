# Supr Calculator

This solution is a Next.js based solution.

## Usage

### Task 1:
```
$ node --experimental-modules validator.mjs
```

### Task 2:
```
$ node --experimental-modules evaluator.mjs
```

### Task 3 and 4
Prerequisites: Node v11
```
$ npm install
$ npm run dev
```
