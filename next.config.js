const compose = require('compose-function');
const withSass = require('@zeit/next-sass');
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');
const withESLint = require('next-eslint');

module.exports = compose(
	withESLint,
	withSass
)({
	cssModules: true,
	webpack(config) {
		config.module.rules.push({
			test: /\.(png|svg|eot|otf|ttf|woff|woff2)$/,
			use: {
				loader: 'url-loader',
				options: {
					limit: 8192,
					publicPath: '/_next/static',
					outputPath: 'static/'
				},
			},
		});
		if (process.env.ANALYZE) {
			config.plugins.push(
				new BundleAnalyzerPlugin({
					analyzerMode: 'static',
					openAnalyzer: true,
				})
			);
		}
		return config;
	},
});
