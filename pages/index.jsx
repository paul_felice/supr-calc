import React from 'react';
import '../styles/styles.scss';
import { Tab } from 'semantic-ui-react';
import { Container, Row, Column } from '../components/Grid';
import { Evaluator } from '../components/Evaluator';
import { History } from '../components/History';
import { Keypad } from '../components/Keypad';

const panes = [
	{
		menuItem: 'Home',
		render: () => (
			<Tab.Pane attached>
				<Evaluator />
			</Tab.Pane>
		),
	},
	{
		menuItem: 'History',
		render: () => (
			<Tab.Pane attached>
				<History />
			</Tab.Pane>
		),
	},
];

const Index = () => (
	<>
		<Container>
			<Row>
				<Column>
					<Tab
						menu={{ secondary: true, pointing: true }}
						panes={panes}
					/>
				</Column>
			</Row>
		</Container>
		<Keypad />
	</>
);

export default Index;
