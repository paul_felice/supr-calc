import axios from 'axios';

class Stack extends Array {
	top() {
		return this[this.length - 1];
	}

	empty() {
		return !this.length;
	}
}

const precedence = op => {
	if (op === '+' || op === '-') return 1;
	if (op === '*' || op === '/') return 2;
	return 0;
};

const isFunction = input =>
	typeof input === 'string' &&
	!!['tan', 'sin', 'cos'].find(x => {
		const reg = new RegExp(`^${x}\\([0-9]+\\)`);
		return input.match(reg);
	});

const evaluateFunction = input => {
	const match = input.match(/^([a-z]{3})\(([0-9]+)\)/);
	const func = match[1];
	const operand = (match[2] * Math.PI) / 180;
	switch (func) {
		case 'sin':
			return Math.sin(operand);
		case 'cos':
			return Math.cos(operand);
		case 'tan':
			return Math.tan(operand);
		default:
			return input;
	}
};

// Function to perform arithmetic operations.
const applyOp = (a, b, op) => {
	if (![a, b, op].every(x => x !== undefined)) throw new Error('Invalid Expression');

	switch (op) {
		case '+':
			return a + b;
		case '-':
			return a - b;
		case '*':
			return a * b;
		case '/':
			return a / b;
		default:
			throw new Error(`Operator invalid: ${op}`);
	}
};

// Function that returns value of
// expression after evaluation.
export const evaluate = async inputString => {
	let tokens = inputString;

	// check special case where input starts with + or -
	if (tokens.match(/^\s*[+\\-]/)) {
		tokens = `0${tokens}`;
	}

	// stack to store integer values.
	const values = new Stack();

	// stack to store operators.
	const ops = new Stack();

	const separatedTokens = [];
	while (tokens.length) {
		if (tokens.match(/^[0-9]+/)) {
			const match = tokens.match(/^[0-9]+/);
			separatedTokens.push(match[0]);
			tokens = tokens.substring(match[0].length);
			continue;
		} else if (tokens.match(/^[+\-*/]/)) {
			const match = tokens.match(/^[+\-*/]/);
			separatedTokens.push(match[0]);
			tokens = tokens.substring(match[0].length);
			continue;
		} else if (tokens.match(/^sin\([0-9]+\)/)) {
			const match = tokens.match(/^sin\([0-9]+\)/);
			separatedTokens.push(match[0]);
			tokens = tokens.substring(match[0].length);
			continue;
		} else if (tokens.match(/^cos\([0-9]+\)/)) {
			const match = tokens.match(/^cos\([0-9]+\)/);
			separatedTokens.push(match[0]);
			tokens = tokens.substring(match[0].length);
			continue;
		} else if (tokens.match(/^tan\([0-9]+\)/)) {
			const match = tokens.match(/^tan\([0-9]+\)/);
			separatedTokens.push(match[0]);
			tokens = tokens.substring(match[0].length);
			continue;
		}  else if (tokens.match(/^RAND/)) {
			const match = tokens.match(/^RAND/);
			separatedTokens.push(match[0]);
			tokens = tokens.substring(match[0].length);
			continue;
		} else if (tokens.match(/^ +/)) {
			const match = tokens.match(/^ +/);
			tokens = tokens.substring(match[0].length);
			continue;
		}
		throw new Error('Invalid expression');
	}

	await Promise.all(separatedTokens.map(async token => {
		if (!isNaN(token)) {
			values.push(parseInt(token, 10));
		} else if (isFunction(token)) {
			values.push(evaluateFunction(token));
		} else if (token === 'RAND') {
			const random = await axios.get('https://www.random.org/integers/?num=1&min=1&max=100&col=1&base=10&format=plain&rnd=new');
			values.push(parseInt(random.data, 10));
		}

		// Current token is an operator.
		else {
			// While top of 'ops' has same or greater
			// precedence to current token, which
			// is an operator. Apply operator on top
			// of 'ops' to top two elements in values stack.
			while (!ops.empty() && precedence(ops.top()) >= precedence(token)) {
				const val2 = values.top();
				values.pop();

				const val1 = values.top();
				values.pop();

				const op = ops.top();
				ops.pop();

				values.push(applyOp(val1, val2, op));
			}

			// Push current token to 'ops'.
			ops.push(token);
		}
	}));

	// Entire expression has been parsed at this
	// point, apply remaining ops to remaining
	// values.
	while (!ops.empty()) {
		const val2 = values.top();
		values.pop();

		const val1 = values.top();
		values.pop();

		const op = ops.top();
		ops.pop();

		values.push(applyOp(val1, val2, op));
	}

	// Top of 'values' contains result, return it.
	return values.top();
};